module.exports = {
  mode: 'jit',
  purge: ['./src/**/*.html', './src/**/*.js'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      roboto: ['"Roboto"', 'sans-serif'],
    },
    screens: {
      sm: '640px',
      // => @media (min-width: 640px) { ... }

      md: '768px',
      // => @media (min-width: 768px) { ... }

      lg: '1024px',
      // => @media (min-width: 1024px) { ... }

      xl: '1220px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
    },
    fontSize: {
      xs: '12px',
      sm: '14px',
      base: '16px',
      md: '18px',
      lg: '20px',
      xl: '24px',
      '2xl': '28px',
      '3xl': '36px',
      '4xl': '48px',
      '5xl': '64px',
    },
    extend: {
      colors: {
        black: {
          500: '#000000',
          primary: '#141514',
          body: '#313131',
          line: '#D9DBE9',
        },
        red: {
          primary: '#E41C39',
          dark: '#38080E',
          medium: '#6F0E1C',
          light: '#EA2B49',
        },
        blue: {
          50: '#00418A',
          100: '#003570',
          200: '#002957',
          300: '#001D3D',
          400: '#001124',
          500: '#00050a',
          ornament: '#011D3D',
        },
      },
      flex: {
        detail: '1 3',
      },
      lineHeight: {
        1.4: '1.4',
      },
      animation: {
        'spin-slow': 'spin 20s linear infinite',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    // require('@tailwindcss/forms')
  ],
};
