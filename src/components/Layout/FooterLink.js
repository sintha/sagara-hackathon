import propTypes from 'prop-types';
import Link from 'next/link';

const FooterLink = ({ href, children, newTab = false }) => {
  return (
    <Link passHref href={href}>
      {newTab ? (
        <a
          target="_blank"
          rel="noopener noreferrer"
          className="relative pb-2 w-maxtext-white text-sm font-normal border-b border-transparent hover:border-white transition-colors lg:text-lg"
        >
          {children}
        </a>
      ) : (
        <a className="relative pb-2 w-max text-white text-sm font-normal border-b border-transparent hover:border-white transition-colors lg:text-lg">
          {children}
        </a>
      )}
    </Link>
  );
};

FooterLink.propTypes = {
  children: propTypes.node.isRequired,
  href: propTypes.string.isRequired,
  newTab: propTypes.bool,
};

export default FooterLink;
