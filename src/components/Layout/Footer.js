import React from 'react';
import { Instagram, Twitter, Youtube } from '../Icon';

const Footer = () => {
  const toWhatsapp =
    'https://api.whatsapp.com/send?phone=628123456789&text=Hello, Sagara. Mau tanya-tanya soal Hackathon, dong!';

  return (
    <footer class="mt-7 px-3 py-4 md:px-3 md:py-8 bg-black-primary dark:bg-black-primary text-2 text-white dark:text-gray-200 transition-colors duration-200 border-t border-black-body border-opacity-25">
      <div class="flex flex-col">
        <div class="md:hidden mx-auto w-11 h-px rounded-full"></div>
        <div class="mt-4 md:mt-0 flex flex-col md:flex-row">
          <nav class="flex-1 flex flex-col items-center justify-center md:items-end md:border-r border-gray-100 md:pr-5">
            <a
              aria-current="page"
              href="#"
              class="hover:text-gray-300 dark:hover:text-white"
            >
              Schedule
            </a>
            <a aria-current="page" href={toWhatsapp} target="_blank">
              Contact Whatsapp
            </a>
            <a
              aria-current="page"
              href="#"
              class="hover:text-gray-300 dark:hover:text-white"
            >
              Customization
            </a>
          </nav>
          <div class="md:hidden mt-4 mx-auto w-11 h-px rounded-full"></div>
          <div class="mt-4 md:mt-0 flex-1 flex items-center justify-center md:border-r border-gray-100">
            <a
              class="hover:text-primary-gray-20"
              href="https://instagram.com/sagaratechnology"
            >
              <Instagram />
            </a>
            <a class="ml-4 hover:text-primary-gray-20" href="#">
              <Youtube />
            </a>
            <a class="ml-4 hover:text-primary-gray-20" href="#">
              <Twitter />
            </a>
          </div>
          <div class="md:hidden mt-4 mx-auto w-11 h-px rounded-full "></div>
          <div class="mt-0 flex-1 flex md:flex-col items-center justify-center md:items-start md:pl-5 flex-row">
            <span class="">© 2021</span>
            <span class="mt-0 md:mt-7 ml-2 md:ml-0">
              <a
                class="hover:text-gray-700"
                href="https:/www.sagaratechnology.com"
              >
                Sagara Technology
              </a>
            </span>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
