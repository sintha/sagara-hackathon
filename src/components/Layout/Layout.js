import React from 'react';
import Header from './Header';
import Footer from './Footer';

const Layout = ({ children }) => {
  return (
    <div className="min-h-screen bg-black-primary text-white">
      <Header />
      {children}
      <Footer />
    </div>
  );
};

export default Layout;
