import Link from 'next/link';

const Header = () => {
  return (
    <header
      id="header"
      className="flex flex-row justify-between items-center px-4 md:px-10"
    >
      <Link href="/">
        <img
          src="img/sagara-tech.png"
          alt="brand"
          className="h-16 md:h-32 cursor-pointer"
        />
      </Link>
    </header>
  );
};

export default Header;
