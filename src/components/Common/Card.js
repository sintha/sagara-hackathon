import clsx from 'clsx';

const Card = ({ className, variant, title, text }) => {
  return (
    <div
      className={clsx(
        'rounded-md flex flex-col items-center',
        className,
        variant === 'primary' && [
          'text-white text-md text-center border border-red-primary border-opacity-30 p-4 md:px-6 mb-4 md:mb-6 min-w-full md:min-w-0 flex-1',
        ]
      )}
    >
      <h2 className="text-xl mb-2 font-bold title-shadow text-white tracking-wider">
        {title}
      </h2>
      <p className="font-light">{text}</p>
    </div>
  );
};

export default Card;
