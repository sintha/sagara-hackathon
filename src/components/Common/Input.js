import React from 'react';
import clsx from 'clsx';

const Input = ({ placeholder, type, className, size, onChange }) => {
  return (
    <input
      placeholder={placeholder}
      type={type}
      className={clsx(
        'rounded-md py-2 px-2 border outline-none capitalize md:px-4 md:py-3 text-black-primary',
        className
      )}
      onChange={onChange}
    />
  );
};

export default Input;
