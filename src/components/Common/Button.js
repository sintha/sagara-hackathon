import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';

const Button = ({ children, onClick, className, variant, size }) => {
  return (
    <button
      className={clsx(
        'rounded-sm',
        className,
        variant === 'primary' && [
          'text-white bg-red-primary hover:bg-red- active:scale-95 uppercase',
        ],
        variant === 'primary-outline' && [
          'text-red-primary bg-white hover:bg-red-primary border-red-primary border hover:border-white hover:text-white  active:scale-95 uppercase',
        ],
        variant === 'primary-underlined' && [
          'text-red-primary uppercase underline',
        ],
        variant === 'dark' && [
          'transition-all px-4 py-2  font-light text-white bg-gray-800 hover:bg-gray-600 active:scale-95 uppercase',
        ],
        variant === 'dark-outline' && [
          'transition-all px-4 py-2  font-light text-black-primary bg-white  hover:bg-gray-700 border-gray-700 border hover:border-transparennt hover:text-white  active:scale-95 uppercase',
        ],
        size === 'base' && ['px-4 lg:px-6 py-3 lg:py-3 text-xs lg:text-base '],
        size === 'sm' && ['px-2 py-1 text-sm'],
        size === 'large' && ['px-2 py-1 lg:px-6 lg:py-4 text-lg']
      )}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  variant: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.string,
  onClick: PropTypes.func,
};

export default Button;
