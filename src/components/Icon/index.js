import Instagram from './Instagram';
import Youtube from './Youtube';
import Twitter from './Twitter';
import Calendar from './Calendar';
import Location from './Location';

export { Instagram, Youtube, Twitter, Calendar, Location };
