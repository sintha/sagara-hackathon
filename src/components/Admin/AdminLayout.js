import AdminNav from './AdminNav';

const AdminLayout = ({ children }) => {
  return (
    <div className="min-h-screen bg-white text-black-primary">
      <AdminNav />
      {children}
    </div>
  );
};

export default AdminLayout;
