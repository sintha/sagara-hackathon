import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { Input } from '../Common';
import axios from 'axios';
import { api } from '@/data/index';

const ParticipantTable = () => {
  const [people, setPeople] = useState([]);
  const [search, setSearch] = useState('');

  const getAll = () => {
    axios
      .get(`${api}/participants`)
      .then((res) => {
        console.log(res.data);
        setPeople(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  const roles = [
    { label: 'All', value: 'all' },
    { label: 'Project Manager', value: 'project_manager' },
    { label: 'Product Manager', value: 'product_manager' },
    { label: 'Fullstack Enginer', value: 'fullstack' },
    { label: 'Backend Engineer', value: 'backend' },
    { label: 'Frontend Engineer', value: 'frontend' },
    { label: 'UI/UX', value: 'uiux' },
    { label: 'QA Engineer', value: 'qa_engineer' },
  ];

  return (
    <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8 mt-4">
      <div className="flex flex-col">
        <div className="flex items-center justify-center">
          <div className="flex flex-row my-4 py-2 px-4 items-end gap-7">
            <div className="flex flex-col">
              <Input
                type="text"
                placeholder="Search"
                onChange={(event) => {
                  setSearch(event.target.value);
                }}
              />
            </div>
            <div className="flex flex-col">
              <label htmlFor="availability" className="text-sm">
                Availability
              </label>
              <select
                id="availability"
                className="px-2 py-1 h-12 w-40 rounded-md"
              >
                <option value="all">All</option>
                <option value="Available">Available</option>
                <option value="Unavailable">Unavailable</option>
              </select>
            </div>
            <div className="flex flex-col">
              <label htmlFor="role" className="text-sm">
                Role
              </label>
              <select id="role" className="px-2 py-1 h-12 w-40 rounded-md">
                {roles.map((role) => {
                  return <option value={role.value}>{role.label}</option>;
                })}
              </select>
            </div>
            <div className="flex flex-col">
              <label htmlFor="technology" className="text-sm">
                Tech Stack
              </label>
              <select
                className="px-2 py-1 h-12 w-40 rounded-md"
                name="Technology"
              >
                <option value="all">All</option>
                <option value="swift">Swift</option>
                <option value="flutter">Flutter</option>
                <option value="vue">Vue</option>
              </select>
            </div>
            <div className="flex flex-col">
              <label htmlFor="location" className="text-sm">
                Location
              </label>
              <select className="px-2 py-1 h-12 w-40 rounded-md">
                <option value="all">All</option>
                <option value="swift">Semarang</option>
                <option value="flutter">Bandung</option>
              </select>
            </div>
          </div>
        </div>
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Name
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Role
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Availability
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Technology
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Location
                    </th>
                    <th scope="col" className="relative px-6 py-3">
                      <span className="sr-only">Edit</span>
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {people.map((person) =>
                    person.specialization
                      .toLocaleLowerCase()
                      .includes(search.toLocaleLowerCase()) ? (
                      <tr key={person._id}>
                        <td className="px-6 py-4 whitespace-nowrap">
                          <div className="flex items-center">
                            <div className="flex-shrink-0 h-10 w-10">
                              <img
                                className="h-10 w-10 rounded-full"
                                src={person.image}
                                alt=""
                              />
                            </div>
                            <div className="ml-4">
                              <Link href={`/admin/participant/${person.id}`}>
                                <div className="text-sm font-medium text-gray-900 cursor-pointer">
                                  {person.name}
                                </div>
                              </Link>
                              <div className="text-sm text-gray-500">
                                {person.email}
                              </div>
                            </div>
                          </div>
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap">
                          <div className="text-sm text-gray-900">
                            {person.specialization}
                          </div>
                          <div className="text-sm text-gray-500">
                            {person.numExperience > 1
                              ? `${person.numExperience} years`
                              : `${person.numExperience} year`}
                          </div>
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap">
                          <span
                            className={`px-2 inline-flex text-xs leading-5 font-semibold rounded-full ${
                              person.projectStatus === 'Available'
                                ? 'bg-green-100 text-green-800'
                                : ' bg-gray-50 text-gray-500'
                            }`}
                          >
                            {person.projectStatus}
                          </span>
                        </td>

                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {person.technology.join(', ').slice(0, 20) + '...'}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {person.city}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                          <a
                            href="#"
                            className="text-indigo-600 hover:text-indigo-900"
                          >
                            Edit
                          </a>
                        </td>
                      </tr>
                    ) : null
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ParticipantTable;
