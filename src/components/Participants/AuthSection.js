import React from 'react';
import { SiLinkedin, SiFacebook, SiGoogle } from 'react-icons/si';
import Link from 'next/link';

const AuthSection = () => {
  return (
    <section
      id="auth"
      className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8"
    >
      <div className="max-w-md space-y-8">
        <div>
          <img
            className="mx-auto h-12 w-auto md:h-20"
            src="/img/red-lines.png"
            alt="Workflow"
          />
          <h2 className="mt-6 text-center text-md md:text-2xl font-extrabold text-gray-900">
            Register to continue
          </h2>
        </div>
        <div className="flex items-center justify-center w-full">
          <div className="space-y-4 md:space-y-8">
            <Link href="/register" passHref>
              <div className="grid grid-cols-3 py-3 px-6 bg-white cursor-pointer text-md rounded-md active:scale-95 shadow-md">
                <div className="col-span-1">
                  <SiGoogle size={25} />
                </div>
                <span className="col-span-2 text-center">
                  Sign up using Google
                </span>
              </div>
            </Link>
            <Link href="/register" passHref>
              <div className="grid grid-cols-3 py-3 px-6 bg-white cursor-pointer text-md rounded-md active:scale-95 shadow-md">
                <div className="col-span-1">
                  <SiLinkedin size={25} />
                </div>
                <span className="col-span-2 text-center">
                  Sign up using LinkedIn
                </span>
              </div>
            </Link>
            <Link href="/register" passHref>
              <div className="grid grid-cols-3 py-3 px-6 bg-white cursor-pointer text-md rounded-md active:scale-95 shadow-md">
                <div className="col-span-1">
                  <SiFacebook size={25} />
                </div>
                <span className="col-span-2 text-center">
                  Sign up using Facebook
                </span>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default AuthSection;
