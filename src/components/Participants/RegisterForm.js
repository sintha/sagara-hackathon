import axios from 'axios';
import React, { useState } from 'react';
import { api } from '@/data/index';
import { useForm } from 'react-hook-form';
import { FaAngleLeft } from 'react-icons/fa';
import RegisterSummary from './RegisterSummary';

const RegisterForm = () => {
  const [formStep, setFormStep] = useState(0);
  const max_steps = 3;

  const {
    watch,
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({ mode: 'all' });

  const completeFormSet = () => {
    setFormStep((cur) => cur + 1);
  };

  const goPrevStep = () => {
    setFormStep((cur) => cur - 1);
  };

  const renderButton = () => {
    if (formStep > 2) {
      return undefined;
    } else if (formStep === 2) {
      return (
        <button
          disabled={!isValid}
          type="submit"
          className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-primary hover:bg-red-light focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-light disabled:bg-gray-400 disabled:ring-gray-400 disabled:hover:bg-gray-400 disabled:cursor-not-allowed"
        >
          Register
        </button>
      );
    } else {
      return (
        <button
          disabled={!isValid}
          onClick={completeFormSet}
          type="button"
          className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-primary hover:bg-red-light focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-light disabled:bg-gray-400 disabled:ring-gray-400 disabled:hover:bg-gray-400 disabled:cursor-not-allowed"
        >
          Next
        </button>
      );
    }
  };

  const submitForm = (value) => {
    axios.post(`${api}/participants`, { id, image, ...value });
    completeFormSet();
  };

  const id = Date.now();
  const image = '/img/black_icon.png';

  const roles = [
    { label: 'Project Manager', value: 'Project Manager' },
    { label: 'Product Manager', value: 'Product Manager' },
    { label: 'Fullstack Enginer', value: 'Fullstack Engineer' },
    { label: 'Backend Engineer', value: 'Backend Engineer' },
    { label: 'Frontend Engineer', value: 'Frontend Engineer' },
    { label: 'UI/UX', value: 'UI/UX' },
    { label: 'QA Engineer', value: 'QA Engineer' },
  ];

  const techStack = [
    {
      category: 'Frontend',
      skills: ['JavaScript', 'ReactJS', 'VueJS', 'Flutter'],
    },
    {
      category: 'Backend',
      skills: ['Golang', 'Phyton', 'NodeJS'],
    },
    {
      category: 'Design',
      skills: ['Figma', 'Adobe Ilustrator', 'Photoshop'],
    },
  ];

  return (
    <>
      <div className="min-h-full pt-4 px-8 md:px-32">
        <div className="flex flex-col">
          {formStep < max_steps && (
            <div className="px-4 md:px-10 mb-2 md:mb-4 flex flex-row items-center">
              {formStep > 0 && (
                <button
                  className="mr-2 md:mr-4"
                  onClick={goPrevStep}
                  type="button"
                >
                  <FaAngleLeft color="gray" />
                </button>
              )}
              <p className="text-sm md:text-base text-gray-500 ">
                {formStep + 1} of {max_steps}
              </p>
            </div>
          )}
          {formStep < max_steps && (
            <div className="px-4 md:px-10">
              <h3 className="text-lg font-medium leading-6 text-gray-900">
                Profile
              </h3>
              <p className="mt-1 text-sm text-gray-600">
                This information will not be displayed publicly.
              </p>
            </div>
          )}
          <div className="mt-2 md:mt-4">
            <form onSubmit={handleSubmit(submitForm)}>
              {formStep === 0 && (
                <section>
                  <div className="px-4 md:px-10 mt-4 md:mt-7">
                    <label
                      htmlFor="specialization"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Role
                    </label>
                    <select
                      id="specialization"
                      autoComplete="specialization"
                      className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-red-loght sm:text-sm h-12 mb-1"
                      {...register('specialization')}
                    >
                      {roles.map((role) => {
                        return <option value={role.value}>{role.label}</option>;
                      })}
                    </select>
                    <label className="block text-sm font-medium text-gray-700">
                      Your Resume
                    </label>
                    <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                      <div className="space-y-1 text-center">
                        <svg
                          className="mx-auto h-12 w-12 text-gray-400"
                          stroke="currentColor"
                          fill="none"
                          viewBox="0 0 48 48"
                          aria-hidden="true"
                        >
                          <path
                            d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                            strokeWidth={2}
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                        </svg>
                        <div className="flex text-sm text-gray-600">
                          <label
                            htmlFor="cv"
                            className="relative cursor-pointer bg-white rounded-md font-medium text-red-primary hover:text-gray-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-red-primary"
                          >
                            <span>Upload a file</span>
                            <input
                              id="cv"
                              name="cv"
                              type="file"
                              className="sr-only"
                            />
                          </label>
                          <p className="pl-1">or drag and drop</p>
                        </div>
                        <p className="text-xs text-gray-500">
                          pdf, jpg, docx up to 10MB
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="mt-8 md:mt-14">
                    <div className="flex flex-col">
                      <div className="px-4 md:px-10 mb-4 md:mb-8">
                        <h3 className="text-lg font-medium leading-6 text-gray-900">
                          Personal Information
                        </h3>
                        <p className="mt-1 text-sm text-gray-600">
                          Use a permanent address where you can receive mail.
                        </p>
                      </div>
                    </div>
                    <div className="mx-4 md:mx-10">
                      <div className="mb-1 md:mb-4">
                        <label
                          htmlFor="name"
                          className="block text-sm font-medium text-gray-700"
                        >
                          Full Name
                        </label>
                        <input
                          type="text"
                          id="name"
                          autoComplete="name"
                          className="mt-1 focus:ring-gray-500 focus:border-gray-500 block w-full shadow-sm sm:text-sm border-gray-300 text-left rounded-md py-2 px-2 border outline-none capitalize md:px-4 md:py-3 text-black-primary"
                          {...register('name', { required: true })}
                        />
                        {errors.name && (
                          <p className="text-sm text-red-light mt-1 md:mt-2">
                            Name is required.
                          </p>
                        )}
                      </div>

                      <div className="mb-1 md:mb-4">
                        <label
                          htmlFor="education"
                          className="block text-sm font-medium text-gray-700"
                        >
                          Education / University
                        </label>
                        <input
                          type="text"
                          id="education"
                          autoComplete="email"
                          className="mt-1 focus:ring-gray-500 focus:border-gray-500 block w-full shadow-sm sm:text-sm border-gray-300 text-left rounded-md py-2 px-2 border outline-none capitalize md:px-4 md:py-3 text-black-primary"
                          {...register('education', { required: true })}
                        />
                        {errors.education && (
                          <p className="text-sm text-red-light mt-1 md:mt-2">
                            Education is required.
                          </p>
                        )}
                      </div>
                      <div className="mb-1 md:mb-4">
                        <label
                          htmlFor="workingStatus"
                          className="block text-sm font-medium text-gray-700"
                        >
                          Occupation
                        </label>
                        <select
                          id="workingStatus"
                          autoComplete="workingStatus"
                          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 sm:text-sm h-12 mb-1"
                          {...register('workingStatus')}
                        >
                          <option value="Student">Student</option>
                          <option value="Employed">Employed</option>
                          <option value="Unemployed">Unemployed</option>
                        </select>
                      </div>

                      <div className="mb-1 md:mb-4">
                        <label
                          htmlFor="numExperience"
                          className="block text-sm font-medium text-gray-700"
                        >
                          Experience
                        </label>
                        <input
                          type="number"
                          min="0"
                          id="numExperience"
                          autoComplete="numExperience"
                          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 sm:text-sm h-12 mb-1"
                          {...register('numExperience', { required: true })}
                        />
                        {errors.numExperience && (
                          <p className="text-sm text-red-light mt-1 md:mt-2">
                            Please enter your year of experience.
                          </p>
                        )}
                      </div>

                      <div className="mb-1 md:mb-4">
                        <label
                          htmlFor="email"
                          className="block text-sm font-medium text-gray-700"
                        >
                          Email
                        </label>
                        <input
                          type="text"
                          id="email"
                          autoComplete="postal-code"
                          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 sm:text-sm h-12 mb-1"
                          {...register('email', { required: true })}
                        />
                        {errors.email && (
                          <p className="text-sm text-red-light mt-1 md:mt-2">
                            Email is required.
                          </p>
                        )}{' '}
                      </div>
                      <div className="mb-1 md:mb-4">
                        <label
                          htmlFor="phone"
                          className="block text-sm font-medium text-gray-700"
                        >
                          Phone
                        </label>
                        <input
                          type="text"
                          id="phone"
                          autoComplete="phone"
                          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 sm:text-sm h-12 mb-1"
                          {...register('phone', { required: true })}
                        />
                        {errors.phone && (
                          <p className="text-sm text-red-light mt-1 md:mt-2">
                            Phone number is required.
                          </p>
                        )}{' '}
                      </div>
                      <div className="mb-1 md:mb-4">
                        <label
                          htmlFor="address"
                          className="block text-sm font-medium text-gray-700"
                        >
                          Address
                        </label>
                        <input
                          type="text"
                          id="address"
                          autoComplete="address"
                          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 sm:text-sm h-12 mb-1"
                          {...register('address', { required: true })}
                        />
                        {errors.address && (
                          <p className="text-sm text-red-light mt-1 md:mt-2">
                            Address is required.
                          </p>
                        )}{' '}
                      </div>
                      <div className="mb-1 md:mb-4">
                        <label
                          htmlFor="city"
                          className="block text-sm font-medium text-gray-700"
                        >
                          City / Regency
                        </label>
                        <input
                          type="text"
                          id="city"
                          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 sm:text-sm h-12 mb-1"
                          {...register('city', { required: true })}
                        />
                        {errors.city && (
                          <p className="text-sm text-red-light mt-1 md:mt-2">
                            City is required.
                          </p>
                        )}
                      </div>
                      <div className="mb-2 md:mb-4">
                        <label
                          htmlFor="linkedin"
                          className="block text-sm font-medium text-gray-700"
                        >
                          LinkedIn
                        </label>
                        <div className="mt-1 flex rounded-md shadow-sm">
                          <span className="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                            https://
                          </span>
                          <input
                            type="text"
                            id="linkedin"
                            className="focus:ring-gray-500 flex-1  rounded-none rounded-r-md border-gray-300 block w-full py-2 px-3 border  bg-white shadow-sm focus:outline-none  focus:border-gray-500 sm:text-sm h-12 "
                            placeholder="linkedin.com/in/you"
                            {...register('linkedin')}
                          />
                        </div>
                        <div className="mt-1 md:mt-4 mb-1 md:mb-4">
                          <label
                            htmlFor="projectStatus"
                            className="block text-sm font-medium text-gray-700"
                          >
                            Are you available for projects?
                          </label>
                          <select
                            id="projectStatus"
                            autoComplete="projectStatus"
                            className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 sm:text-sm h-12 mb-1"
                            {...register('projectStatus')}
                          >
                            <option defaultValue="Available">Available</option>
                            <option value="Unavailable">Unavailable</option>
                          </select>
                        </div>
                      </div>
                      <div className="hidden sm:block" aria-hidden="true">
                        <div className="py-5">
                          <div className="border-t border-gray-200" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="px-4 md:px-10 mt-8 md:mt-10">
                    <div className="mb-4 md:mb-8">
                      <h3 className="text-lg font-medium leading-6 text-gray-900">
                        Tech Stack
                      </h3>
                      <p className="mt-1 text-sm text-gray-600">
                        Please add your skills.
                      </p>
                    </div>
                    <div className="flex items-start mt-2 md:mt-4"></div>
                    <div>
                      {techStack &&
                        techStack.map((item) => {
                          return (
                            <>
                              <div>
                                <div className="mt2 md:mt-7">
                                  <p className="text-sm md:text-md font-medium leading-6 text-gray-900">
                                    {item.category}
                                  </p>
                                </div>
                                <div className="grid grid-cols-2 md:grid-cols-6 mt-2 md:mt-4 gap-4 md:gap-6 mb-4 md:mb-6">
                                  {item.skills.map((skill) => {
                                    return (
                                      <>
                                        <div className="flex items-center h-5">
                                          <input
                                            id={skill}
                                            type="checkbox"
                                            value={skill}
                                            className="focus:ring-gray-500 h-4 w-4 text-gray-500 border-gray-300 rounded mr-1 md:mr-2"
                                            {...register('technology')}
                                          />
                                          <label
                                            htmlFor={skill}
                                            className="font-base md:font-medium text-gray-700"
                                          >
                                            {skill}
                                          </label>
                                        </div>
                                      </>
                                    );
                                  })}
                                </div>
                              </div>
                            </>
                          );
                        })}
                    </div>
                    <div className="mt-2 md:mt-4">
                      <div className="mb-1 md:mb-4">
                        <label
                          htmlFor="works"
                          className="block text-sm font-medium text-gray-700"
                        >
                          Your Project Link
                        </label>
                        <input
                          type="text"
                          id="works"
                          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 sm:text-sm h-12 mb-1"
                          {...register('works')}
                        />
                      </div>
                    </div>
                  </div>
                </section>
              )}
              {formStep === 1 && (
                <section>
                  <div className="px-4 md:px-10">
                    <div className="mb-2 md:mb-4">
                      <h1 className="font-normal text-base md:text-md">
                        Do you have team mates?
                      </h1>
                    </div>
                    <div className="grid grid-cols-1 md:grid-cols-3 gap-2 md:gap-4">
                      <div>
                        <input
                          type="radio"
                          id="solo"
                          value="solo"
                          className="mr-1 md:mr-2"
                          {...register('teamId')}
                        />
                        <label htmlFor="">I am working solo</label>
                      </div>
                      <div>
                        <input
                          type="radio"
                          id="needTeam"
                          value="needTeam"
                          className="mr-1 md:mr-2"
                          {...register('teamId')}
                        />
                        <label htmlFor="needTeam">Looking for Team mates</label>
                      </div>
                      <div>
                        <input
                          type="radio"
                          id="haveTeam"
                          value="haveTeam"
                          className="mr-1 md:mr-2"
                          {...register('teamId')}
                        />
                        <label htmlFor="team">Already have a team</label>
                      </div>
                    </div>
                  </div>
                </section>
              )}
              {formStep === 2 && (
                <section>
                  <RegisterSummary />
                </section>
              )}
              {formStep === 3 && (
                <section>
                  Congratulations, you are registered for Hackathon 2021!
                </section>
              )}
              <div className="px-4 py-3 text-right sm:px-6">
                {renderButton()}
              </div>
              {/* Watch form: */}
              <pre>{JSON.stringify(watch(), null, 2)}</pre>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default RegisterForm;
