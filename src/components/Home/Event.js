import React from 'react';
import { Card } from '../Common';

const Event = () => {
  const cards = [
    {
      title: "What's awesome",
      text: 'This is the largest student hackathon in Indonesia. Hosted by Sagara Technology, builds mission-driven technology that helps people to solve critical real-world problems. We create impact through digital, design, and hardware.',
    },
    {
      title: 'All students welcome!',
      text: "Whether it's your first hackathon or you're an experienced hacker, Hack-a-day is perfect for you and there's no entry fee. You can join with your team, or we can arrange you a team!",
    },
    {
      title: 'Expect great things',
      text: 'We’ll have mentors from different companies, and a great atmosphere for learning something new. We’ll have many activities and ways to communicate to make the best out of this event for you!',
    },
  ];

  return (
    <section id="event">
      <div className="px-4 py-4 md:py-10 md:px-16 relative">
        <div className="flex flex-col md:flex-row md:gap-14 gap-4">
          {cards.map((item) => {
            return (
              <Card variant="primary" title={item.title} text={item.text} />
            );
          })}
        </div>
        <div className="absolute z-0 -bottom-32 -right-20">
          <img
            src="pattern/geometrics.svg"
            alt=""
            className="opacity-10 w-52 md:w-96"
          />
        </div>
      </div>
    </section>
  );
};

export default Event;
