import React, { useEffect, useRef, useState } from 'react';

const Countdown = () => {
  const [timerDays, setTimerDays] = useState('00');
  const [timerHours, setTimerHours] = useState('00');
  const [timerMinutes, setTimerMinutes] = useState('00');
  const [timerSeconds, setTimerSeconds] = useState('00');

  let interval = useRef();

  const startTimer = () => {
    const countDownDate = new Date('November 1, 2021 08:51:00').getTime();
    interval = setInterval(() => {
      const now = new Date().getTime();
      const distance = countDownDate - now;

      const days = Math.floor(distance / (1000 * 60 * 60 * 24));
      const hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);

      if (distance < 0) {
        clearInterval(interval.current);
      } else {
        setTimerDays(days);
        setTimerHours(hours);
        setTimerMinutes(minutes);
        setTimerSeconds(seconds);
      }
    }, 1000);
  };

  useEffect(() => {
    startTimer();
    return () => {
      clearInterval(interval.current);
    };
  });

  return (
    <section className="flex flex-row items-center justify-around p-4 md:p-7">
      <div className="flex flex-col items-center justify-center">
        <p className="text-md md:text-3xl">{timerDays}</p>
        <p className="text-base font-light">Days</p>
      </div>
      <span>:</span>
      <div className="flex flex-col items-center justify-center">
        <p className="text-md md:text-3xl">{timerHours}</p>
        <p className="text-base font-light">Hours</p>
      </div>
      <span>:</span>
      <div className="flex flex-col items-center justify-center">
        <p className="text-md md:text-3xl">{timerMinutes}</p>
        <p className="text-base font-light">Minutes</p>
      </div>
      <span>:</span>
      <div className="flex flex-col items-center justify-center">
        <p className="text-md md:text-3xl">{timerSeconds}</p>
        <p className="text-base font-light">Seconds</p>
      </div>
    </section>
  );
};

export default Countdown;
