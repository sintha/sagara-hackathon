import React from 'react';
import Link from 'next/link';
import { Button } from '../Common';
import { Calendar, Location } from '../Icon';
import { CalendarIcon, MapIcon } from '@heroicons/react/solid';
import Countdown from './Countdown';

const Hero = () => {
  return (
    <section id="hero">
      <div className="min-h-screen">
        <div className="relative z-0">
          <div className="overflow-hidden absolute top-10 md:top-30 -left-32 z-0 ">
            <img
              src="pattern/circle.svg"
              alt=""
              className="w-96 md:w-[600px] opacity-25 md:opacity-30 animate-spin-slow"
            />
          </div>
        </div>
        <div className="flex flex-col items-center">
          <div className="max-w-md md:max-w-xl flex items-center justify-center">
            <img src="pattern/hero.svg" alt="" className="w-2/3" />
          </div>
          <div className="flex flex-col items-center justify-center w-full text-md md:text-xl">
            <div className="w-full md:w-1/2 my-2 p-2 md:p-7">
              <div className="h-auto bg-gray-500 bg-opacity-5">
                <Countdown />
              </div>
            </div>
            <div className="flex flex-row items-center justify-center mt-4">
              <div className="flex flex-row items-center justify-center">
                <Calendar />
                &nbsp;
                <p> 1 November 2021</p>
              </div>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <div className="flex flex-row items-center justify-center">
                <Location />
                &nbsp;
                <p> Online</p>
              </div>
            </div>
          </div>
          <div className="flex flex-col mt-4 mb-10 md:mb-20">
            <div className="my-4 md:px-20 px-4">
              <p className="font-light text-md md:text-xl text-center">
                Hack-a-day is proud to be the largest 24-hour hackathon in
                Indonesia, offering engaging tech talks, interactive demos, and
                great food! Join us to unleash your inner innovator!
              </p>
            </div>
            <div className="flex flex-col items-center my-4 md:my-9">
              <Link passHref href="/register">
                <Button
                  variant="primary"
                  size="large"
                  className="px-10 py-4 hidden md:block"
                >
                  Register Now
                </Button>
              </Link>
              <Link passHref href="/authentication">
                <Button
                  variant="primary"
                  size="base"
                  className="block md:hidden"
                >
                  Register Now
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
