import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import Axios from 'axios';
import { useRouter } from 'next/router';
import { participants } from '@/data/variabels/participants';
import AdminLayout from '@/components/Admin/AdminLayout';
import { api } from '@/data/index';
import { Button } from '@/components/Common';

const Participant = () => {
  const route = useRouter();
  const { participant } = route.query;
  const [people, setPeople] = useState([]);

  const fetchPeople = () =>
    Axios.get(`${api}/participants`)
      .then((res) => {
        setPeople(res.data);
      })
      .catch((err) => {
        console.log(err);
      });

  useEffect(() => {
    fetchPeople();
  }, []);

  const person = participants.find((item) => item.id == participant);
  const telephone = person.phone;
  const message = `https://api.whatsapp.com/send?phone=${telephone}&text=Halo.%20Kami%20dari%20Sagara%20Technology%2C%20apakah%20bersedia%20bergabung%20untuk%20project%3F`;

  const rows = ({ children, className }) => {
    <div className={clsx('my-2 grid grid-cols-2 gap-7', className)}>
      {children}
    </div>;
  };

  return (
    <AdminLayout>
      <div className="min-h-screen w-full flex bg-gray-50 py-4 px-2 md:px-8">
        <div className="flex flex-row gap-8 justify-between w-full">
          <div className="flex flex-col items-center w-1/3">
            <div className="w-24 mx-2 md:mx-6">
              <img src={person.image} alt="" />
            </div>
            <div className="my-2">
              <p>{person.name}</p>
            </div>
            <div className="flex flex-col">
              <span
                className={`my-2 py-2 text-sm text-center leading-5 font-semibold rounded-md ${
                  person.projectStatus === 'Available'
                    ? 'bg-green-100 text-green-800'
                    : ' bg-gray-50 text-gray-500'
                }`}
              >
                {person.projectStatus} for Project
              </span>
              <Button variant="dark-outline" className="my-2">
                Download CV
              </Button>
              <a href={message} target="_blank">
                <Button variant="dark-outline" className="my-2">
                  Send Message
                </Button>
              </a>
            </div>
          </div>
          <div className="flex flex-col w-2/3">
            <div className="text-md font-normal uppercase my-4">
              <h1>Profile: </h1>
            </div>
            <div className="border-b border-gray-2 my-4 w-full"></div>
            <div className="grid grid-cols-2 gap-7 my-4">
              <p>Email:</p>
              <span>{person.email}</span>
            </div>
            <div className="grid grid-cols-2 gap-7 my-4">
              <p>Title:</p>
              <span>{person.title}</span>
            </div>
            <div className="grid grid-cols-2 gap-7 my-4">
              <p>Specialization:</p>
              <span>{person.specialization}</span>
            </div>
            <div className="grid grid-cols-2 gap-7 my-4">
              <p>Experience:</p>
              <span>
                {person.numExperience}{' '}
                {person.numExperience > 1 ? 'years' : 'year'}
              </span>
            </div>
            <div className="grid grid-cols-2 gap-7 my-4">
              <p>Phone:</p>
              <span>{person.phone}</span>
            </div>
            <div className="grid grid-cols-2 gap-7 my-4">
              <p>Location:</p>
              <span>{person.city}</span>
            </div>
            <div className="grid grid-cols-2 gap-7 my-4">
              <p>Skill:</p>
              <div className="grid auto-rows-auto">
                {person.technology &&
                  person.technology.map((item) => {
                    return <span>{item}</span>;
                  })}
              </div>
            </div>
            <div className="grid grid-cols-2 gap-7 my-4">
              <p>Working Status:</p>
              <span>{person.workingStatus}</span>
            </div>
            <div className="grid grid-cols-2 gap-7 my-4">
              <p>Projects:</p>
              <div className="grid auto-rows-auto">
                {person.works.map((work) => {
                  return (
                    <a href={work} target="_blank">
                      {work}
                    </a>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </AdminLayout>
  );
};

export default Participant;
