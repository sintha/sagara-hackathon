import AdminLayout from '@/components/Admin/AdminLayout';
import ParticipantTable from '@/components/Admin/ParticipantTable';
import React from 'react';

const Participants = () => {
  return (
    <AdminLayout>
      <ParticipantTable />
    </AdminLayout>
  );
};

export default Participants;
