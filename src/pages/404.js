import { Button } from '@/components/Common';
import Header from '@/components/Layout/Header';
import React from 'react';
import Link from 'next/link';
import Image from 'next/image';

const NotFound = () => {
  return (
    <div className="min-h-screen flex-col flex items-center justify-center">
      <div className="p-4 md:p-10">
        <div className="flex-col flex items-center justify-center">
          <div className="mx-auto my-4 lg:my-10">
            <Image src="/img/not-found.png" width={200} height={100}></Image>
          </div>
          <div>
            <p className="text-2xl uppercase">Page not found.</p>
          </div>
          <Link passHref href="/">
            <Button variant="primary-underlined" size="base">
              Go Home
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
