import React from 'react';
import Header from '@/components/Layout/Header';
import RegisterForm from '@/components/Participants/RegisterForm';

const formRegister = () => {
  return (
    <>
      <Header />
      <RegisterForm />
    </>
  );
};

export default formRegister;
