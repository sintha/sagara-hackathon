import React from 'react';
import AuthSection from '@/components/Participants/AuthSection';

const RegisterPage = () => {
  return (
    <>
      <AuthSection />
    </>
  );
};

export default RegisterPage;
