import Layout from '../components/Layout/Layout';
import Hero from '../components/Home/Hero';
import Event from '../components/Home/Event';

export default function Home() {
  return (
    <section id="home">
      <>
        <Layout>
          <Hero />
          <Event />
        </Layout>
      </>
    </section>
  );
}
